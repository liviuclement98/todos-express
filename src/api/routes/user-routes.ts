import express from 'express';

const usersController = require('../controllers/users-controller')

const router = express.Router();

router.post('/authenticate', usersController.login);

// router.get('/:rid/results', activityController.getResultsByRoomCode);
//
// router.post('', activityController.createRoom);
//
// router.post('/:qaid', activityController.createAnswer);
//
// router.patch('/answers', activityController.submitAnswers);

module.exports = router;
