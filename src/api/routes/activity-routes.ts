const express = require('express');

const activityController = require('../controllers/activities-controller')

const router = express.Router();

router.get('/', activityController.getAllActivities);

router.post('/', activityController.addActivity);

router.put('/:aid/', activityController.editActivity);

router.delete('/:aid/', activityController.deleteActivity);

module.exports = router;
