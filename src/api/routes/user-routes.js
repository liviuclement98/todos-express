"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const usersController = require('../controllers/users-controller.js');
const router = express_1.default.Router();
router.post('/authenticate', usersController.login);
// router.get('/:rid/results', activityController.getResultsByRoomCode);
//
// router.post('', activityController.createRoom);
//
// router.post('/:qaid', activityController.createAnswer);
//
// router.patch('/answers', activityController.submitAnswers);
module.exports = router;
