"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const activityController = require('../controllers/activities-controller.js');
const router = express_1.default.Router();
router.get('/', activityController.getAllActivities);
router.post('/', activityController.addActivity);
router.put('/:aid/', activityController.editActivity);
router.delete('/:aid/', activityController.deleteActivity);
module.exports = router;
