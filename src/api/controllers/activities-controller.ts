import {NextFunction, Request, Response} from "express";
import Activity from "../models/Activity";
import Todo from "../models/Todo";
import slackClient from '../../utils/slack/config';

const getAllActivities = (req: Request, res: Response, next: NextFunction) => {
    const {oktaId} = req.userData;

    Activity.findAll({
        where: {
            user_okta_id: oktaId,
        },
        include: {
            model: Todo,
            as: 'toDoList',
        },
        order: [
            [
                {model: Todo, as: 'toDoList'},
                'id',
                'ASC'
            ],
        ],
    })
        .then((activities: any) => {
            res.status(200).json({
                activities
            })
        })
        .catch((error: any) => {
            res.status(500).json({
                message: error.message,
                error,
            })
        })
}

const addActivity = async (req: Request, res: Response, next: NextFunction) => {
    const {oktaId} = req.userData;
    const {title, description, toDoList, status} = req.body;
    let newActivity: any = null;
    let todos = null;

    if (!title || !description || !toDoList || !status) {
        return res.status(404).json({
            message: 'Invalid parameters'
        })
    }

    try {
        newActivity = await Activity.create({
            title, description, status, user_okta_id: oktaId
        })
    } catch (error) {
        console.log({error})
        return res.status(500).json({
            message: 'Could not create activity'
        })
    }

    try {
        if (newActivity && toDoList.length) {
            todos = await Todo.bulkCreate(toDoList.map((toDo: any) => ({
                ...toDo,
                activity_id: newActivity.dataValues.id,
            })))
        }
    } catch (error) {
        return res.status(500).json({
            message: 'Could not add todos'
        })
    }

    let slackText = `:heavy_plus_sign:\nActivity *${newActivity.dataValues.title}* has been added `;

    if (todos?.length) {
        slackText += 'with the following TODOS:';

        todos?.forEach((todo: any) => {
            slackText += `\n - ${todo.dataValues.name}`
        });
    }

    slackClient.chat.postMessage({
        channel: 'todoss',
        text: slackText,
    })
        .catch((error: any) => console.log({error}))

    res.status(200).json({
        newActivity: {
            ...newActivity.dataValues,
            toDoList: todos ? todos.map((toDo: any) => ({
                ...toDo.dataValues,
            })) : []
        }
    })
}

const editActivity = async (req: Request, res: Response, next: NextFunction) => {
    const {aid} = req.params;
    const {title, description, toDoList, status} = req.body;
    let newActivity: any = null;
    let currentTodos = null;
    let todos = null;

    if (!title || !description || !toDoList || !status) {
        return res.status(404).json({
            message: 'Invalid parameters'
        })
    }

    try {
        currentTodos = await Todo.findAll({
            where: {
                activity_id: aid,
            }
        })
    } catch (error) {
        return res.status(500).json({
            message: 'Could not update activity'
        })
    }

    try {
        await Activity.update({
            title, description, status
        }, {
            where: {
                id: aid
            },
            returning: true
        });

        newActivity = await Activity.findByPk(aid);
    } catch (error) {
        return res.status(500).json({
            message: 'Could not update activity'
        })
    }

    const newToDoListLength = toDoList.length;
    const currentTodoIds = currentTodos.map((currentTodo: any) => currentTodo.dataValues.id);
    const newToDoIds = newToDoListLength ? toDoList.map((toDo: any) => parseInt(toDo.id)) : [];
    const removedToDoIds = currentTodoIds.filter((currentToDoId: string) => !newToDoIds.includes(currentToDoId));

    try {
        await Todo.destroy({
            where: {
                id: removedToDoIds,
            }
        })

        if (newToDoListLength) {
            await Todo.bulkCreate(toDoList.map((toDo: any) => ({
                ...toDo,
                activity_id: aid,
            })), {
                updateOnDuplicate: ['name', 'status']
            })
        }

        todos = await Todo.findAll({
            where: {
                activity_id: aid,
            },
            order: [
                ['id', 'ASC'],
            ],
        })
    } catch (error) {
        return res.status(500).json({
            message: 'Could not update todos'
        })
    }

    let slackText = `:pencil2:\nActivity *${newActivity.dataValues.title}* has been modified: \nStatus: ${newActivity.dataValues.status === 'Done' ? '*Done*' : '*Not Done*'}\n`;

    if (todos?.length) {
        slackText += '\nTODOS:\n';

        todos?.forEach((todo: any) => {
            slackText += `\n - ${todo.dataValues.name} :${todo.dataValues.status === 'Done' ? 'white_check_mark' : 'x'}:`
        });
    }

    slackClient.chat.postMessage({
        channel: 'todoss',
        text: slackText,
    })
        .catch((error: any) => console.log({error}))

    res.status(200).json({
        editedActivity: {
            ...newActivity.dataValues,
            toDoList: todos ? todos.map((toDo: any) => ({
                ...toDo?.dataValues,
            })) : []
        }
    })
}

const deleteActivity = async (req: Request, res: Response, next: NextFunction) => {
    const {aid} = req.params;
    let deletedActivity = null;

    try {
        deletedActivity = await Activity.findByPk(aid);

        await Activity.destroy({
            where: {
                id: aid
            },
        });
    } catch (error) {
        return res.status(500).json({
            message: 'Could not delete activity'
        })
    }

    slackClient.chat.postMessage({
        channel: 'todoss',
        text: `:put_litter_in_its_place: Activity ${deletedActivity.dataValues.title} has been deleted`,
    })
        .catch((error: any) => console.log({error}))


    return res.status(200).json({
        deletedActivityId: aid,
    })
}

module.exports = {
    getAllActivities,
    addActivity,
    editActivity,
    deleteActivity,
}
