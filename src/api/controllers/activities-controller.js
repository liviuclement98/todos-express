"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Activity_1 = __importDefault(require("../models/Activity.js"));
const Todo_1 = __importDefault(require("../models/Todo.js"));
const config_1 = __importDefault(require("../../utils/slack/config"));
const getAllActivities = (req, res, next) => {
    const { oktaId } = req.userData;
    Activity_1.default.findAll({
        where: {
            user_okta_id: oktaId,
        },
        include: {
            model: Todo_1.default,
            as: 'toDoList',
        },
        order: [
            [
                { model: Todo_1.default, as: 'toDoList' },
                'id',
                'ASC'
            ],
        ],
    })
        .then((activities) => {
        res.status(200).json({
            activities
        });
    })
        .catch((error) => {
        res.status(500).json({
            message: error.message,
            error,
        });
    });
};
const addActivity = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { oktaId } = req.userData;
    const { title, description, toDoList, status } = req.body;
    let newActivity = null;
    let todos = null;
    if (!title || !description || !toDoList || !status) {
        return res.status(404).json({
            message: 'Invalid parameters'
        });
    }
    try {
        newActivity = yield Activity_1.default.create({
            title, description, status, user_okta_id: oktaId
        });
    }
    catch (error) {
        console.log({ error });
        return res.status(500).json({
            message: 'Could not create activity'
        });
    }
    try {
        if (newActivity && toDoList.length) {
            todos = yield Todo_1.default.bulkCreate(toDoList.map((toDo) => (Object.assign(Object.assign({}, toDo), { activity_id: newActivity.dataValues.id }))));
        }
    }
    catch (error) {
        return res.status(500).json({
            message: 'Could not add todos'
        });
    }
    let slackText = `:heavy_plus_sign:\nActivity *${newActivity.dataValues.title}* has been added `;
    if (todos === null || todos === void 0 ? void 0 : todos.length) {
        slackText += 'with the following TODOS:';
        todos === null || todos === void 0 ? void 0 : todos.forEach((todo) => {
            slackText += `\n - ${todo.dataValues.name}`;
        });
    }
    config_1.default.chat.postMessage({
        channel: 'todoss',
        text: slackText,
    })
        .catch((error) => console.log({ error }));
    res.status(200).json({
        newActivity: Object.assign(Object.assign({}, newActivity.dataValues), { toDoList: todos ? todos.map((toDo) => (Object.assign({}, toDo.dataValues))) : [] })
    });
});
const editActivity = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { aid } = req.params;
    const { title, description, toDoList, status } = req.body;
    let newActivity = null;
    let currentTodos = null;
    let todos = null;
    if (!title || !description || !toDoList || !status) {
        return res.status(404).json({
            message: 'Invalid parameters'
        });
    }
    try {
        currentTodos = yield Todo_1.default.findAll({
            where: {
                activity_id: aid,
            }
        });
    }
    catch (error) {
        return res.status(500).json({
            message: 'Could not update activity'
        });
    }
    try {
        yield Activity_1.default.update({
            title, description, status
        }, {
            where: {
                id: aid
            },
            returning: true
        });
        newActivity = yield Activity_1.default.findByPk(aid);
    }
    catch (error) {
        return res.status(500).json({
            message: 'Could not update activity'
        });
    }
    const newToDoListLength = toDoList.length;
    const currentTodoIds = currentTodos.map((currentTodo) => currentTodo.dataValues.id);
    const newToDoIds = newToDoListLength ? toDoList.map((toDo) => parseInt(toDo.id)) : [];
    const removedToDoIds = currentTodoIds.filter((currentToDoId) => !newToDoIds.includes(currentToDoId));
    try {
        yield Todo_1.default.destroy({
            where: {
                id: removedToDoIds,
            }
        });
        if (newToDoListLength) {
            yield Todo_1.default.bulkCreate(toDoList.map((toDo) => (Object.assign(Object.assign({}, toDo), { activity_id: aid }))), {
                updateOnDuplicate: ['name', 'status']
            });
        }
        todos = yield Todo_1.default.findAll({
            where: {
                activity_id: aid,
            },
            order: [
                ['id', 'ASC'],
            ],
        });
    }
    catch (error) {
        return res.status(500).json({
            message: 'Could not update todos'
        });
    }
    let slackText = `:pencil2:\nActivity *${newActivity.dataValues.title}* has been modified: \nStatus: ${newActivity.dataValues.status === 'Done' ? '*Done*' : '*Not Done*'}\n`;
    if (todos === null || todos === void 0 ? void 0 : todos.length) {
        slackText += '\nTODOS:\n';
        todos === null || todos === void 0 ? void 0 : todos.forEach((todo) => {
            slackText += `\n - ${todo.dataValues.name} :${todo.dataValues.status === 'Done' ? 'white_check_mark' : 'x'}:`;
        });
    }
    config_1.default.chat.postMessage({
        channel: 'todoss',
        text: slackText,
    })
        .catch((error) => console.log({ error }));
    res.status(200).json({
        editedActivity: Object.assign(Object.assign({}, newActivity.dataValues), { toDoList: todos ? todos.map((toDo) => (Object.assign({}, toDo === null || toDo === void 0 ? void 0 : toDo.dataValues))) : [] })
    });
});
const deleteActivity = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { aid } = req.params;
    let deletedActivity = null;
    try {
        deletedActivity = yield Activity_1.default.findByPk(aid);
        yield Activity_1.default.destroy({
            where: {
                id: aid
            },
        });
    }
    catch (error) {
        return res.status(500).json({
            message: 'Could not delete activity'
        });
    }
    config_1.default.chat.postMessage({
        channel: 'todoss',
        text: `:put_litter_in_its_place: Activity ${deletedActivity.dataValues.title} has been deleted`,
    })
        .catch((error) => console.log({ error }));
    return res.status(200).json({
        deletedActivityId: aid,
    });
});
module.exports = {
    getAllActivities,
    addActivity,
    editActivity,
    deleteActivity,
};
