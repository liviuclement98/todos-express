import {NextFunction, Request, Response} from "express";
import User from "../models/User";

const login = async (req: Request, res: Response, next: NextFunction) => {
    const {email, oktaId} = req.body;

    try {
        const existingUser = await User.findOne({
            where: {
                email: email,
            }
        })

        if (!existingUser) {
            await User.create({
                email, okta_id: oktaId,
            });
        }
    } catch (error) {
        console.log({error})
        return res.status(500).json({
            message: 'Could not create user'
        })
    }

    return res.status(200).json({
        message: 'All good dudes'
    })
}

module.exports = {
    login
}
