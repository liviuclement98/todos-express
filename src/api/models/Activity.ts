import User from "./User";
import Todo from "./Todo";

const { DataTypes } = require('sequelize');
const sequelize = require('../../utils/database/sequelize');

const Activity = sequelize.define('activitie', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
    user_okta_id:{
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    timestamps: false,
});

Activity.hasMany(Todo, {as: 'toDoList', foreignKey: 'activity_id'})


export default Activity;
