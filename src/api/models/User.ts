const { DataTypes } = require('sequelize');
const sequelize = require('../../utils/database/sequelize');

const User = sequelize.define('user', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    okta_id: {
        type: DataTypes.STRING,
        allowNull: false
    }
},{
    timestamps: false,
});

export default User;
