import Activity from "./Activity";
const { DataTypes } = require('sequelize');
const sequelize = require('../../utils/database/sequelize');

const Todo = sequelize.define('todo', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
    activity_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
},{
    timestamps: false,
});

// Todo.belongsTo(Activity)

export default Todo;
