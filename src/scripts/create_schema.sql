CREATE DATABASE todos;

CREATE TABLE `todos`.`users` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `email` VARCHAR(60) UNIQUE NOT NULL,
    `okta_id` VARCHAR(64) UNIQUE NOT NULL,
    PRIMARY KEY (`id`, `okta_id`)
);

CREATE TABLE `todos`.`activities` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(60) NOT NULL,
    `description` VARCHAR(200) NOT NULL,
    `status` ENUM('Not Done', 'Done') NOT NULL,
    `user_okta_id` VARCHAR(64) NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`user_okta_id`) REFERENCES users(`okta_id`)
);

CREATE TABLE `todos`.`todos` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(60) NOT NULL,
    `status` ENUM('Not Done', 'Done') NOT NULL,
    `activity_id` INT NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`activity_id`) REFERENCES activities(`id`) ON DELETE CASCADE
);
