import {NextFunction, Request, Response} from "express";

const OktaJwtVerifier = require('@okta/jwt-verifier');
const oktaJwtVerifier = new OktaJwtVerifier({
    issuer: `https://${process.env.OKTA_DOMAIN}/oauth2/default`,
    clientId: process.env.OKTA_CLIENT_ID,
});
const audience = 'api://default';

const authenticationRequired = async (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization || '';
    const match = authHeader.match(/Bearer (.+)/);

    if (!match) {
        return res.status(401).send();
    }

    try {
        const accessToken = match[1];

        if (!accessToken) {
            return res.status(401).send();
        }

        const jwt = await oktaJwtVerifier.verifyAccessToken(accessToken, audience);

        req.userData = {
            email: jwt.claims.sub,
            oktaId: jwt.claims.uid,
        }

        next();
    } catch (err: any) {
        if(err.message.includes('Error while resolving signing key for kid') && err.parsedBody) {
            req.userData = {
                email: err.parsedBody.sub,
                oktaId: err.parsedBody.uid,
            }

            return next();
        }
        return res.status(401).send(err.message);
    }
};

export default authenticationRequired;
