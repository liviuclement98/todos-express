const Sequelize = require('sequelize');
import {DATA_SOURCES} from './config';

const dataSource = DATA_SOURCES.mySqlDataSource;
const {DB_USER, DB_HOST, DB_PASSWORD, DB_DATABASE} = dataSource;

const sequelize = new Sequelize(DB_DATABASE, DB_USER, DB_PASSWORD, {
    host: DB_HOST,
    dialect: 'mysql'
});

module.exports = sequelize;
