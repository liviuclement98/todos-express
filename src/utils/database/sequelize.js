"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Sequelize = require('sequelize');
const config_1 = require("./config.js");
const dataSource = config_1.DATA_SOURCES.mySqlDataSource;
const { DB_USER, DB_HOST, DB_PASSWORD, DB_DATABASE } = dataSource;
const sequelize = new Sequelize(DB_DATABASE, DB_USER, DB_PASSWORD, {
    host: DB_HOST,
    dialect: 'mysql'
});
module.exports = sequelize;
