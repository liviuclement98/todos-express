const User = require('../../api/models/User');
const Activity = require('../../api/models/User');
const Todo = require('../../api/models/User');
const sequelize = require('./sequelize');

const databaseInit = async () => {
    try {
        await sequelize.authenticate();
        await sequelize.sync()

        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
        throw new Error('Unable to connect to the database');
    }
};

export default databaseInit;
