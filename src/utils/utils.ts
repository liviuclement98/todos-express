const globalAny:any = global;

const {
    MYSQL_HOST,
    MYSQL_DATABASE,
    MYSQL_PORT,
    OKTA_DOMAIN,
    OKTA_CLIENT_ID,
    SLACK_TOKEN,
} = process.env;

const autoImportEnvVariables = () => {
    globalAny.DB_HOST = MYSQL_HOST;
    globalAny.DB_NAME = MYSQL_DATABASE;
    globalAny.DB_PORT = MYSQL_PORT;
    globalAny.OKTA_DOMAIN = OKTA_DOMAIN;
    globalAny.OKTA_CLIENT_ID = OKTA_CLIENT_ID;
    globalAny.SLACK_TOKEN = SLACK_TOKEN;
};

export default autoImportEnvVariables;
