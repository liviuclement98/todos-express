"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { WebClient } = require('@slack/web-api');
const slackToken = process.env.SLACK_TOKEN;
const slackClient = new WebClient(slackToken);
exports.default = slackClient;
