const {WebClient} = require('@slack/web-api');

const slackToken = process.env.SLACK_TOKEN;
const slackClient = new WebClient(slackToken)

export default slackClient;
