"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const databaseInit_1 = __importDefault(require("./src/utils/database/databaseInit"));
const authentication_middleware_1 = __importDefault(require("./src/middlewares/authentication-middleware"));
const utils_1 = __importDefault(require("./src/utils/utils"));
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
dotenv_1.default.config();
(0, utils_1.default)();
const app = (0, express_1.default)();
(0, databaseInit_1.default)();
const cors = require('cors');
app.use(cors());
//parse body to json
app.use(express_1.default.json());
const activityRoutes = require('./src/api/routes/activity-routes');
const userRoutes = require('./src/api/routes/user-routes');
app.use('/api/activities', authentication_middleware_1.default, activityRoutes);
app.use('/api/users', userRoutes);
//handle unsupported routes
app.use(() => {
    const error = new Error('Could not find this route');
    error.code = 404;
    throw error;
});
// error handling middleware
app.use((error, req, res, next) => {
    if (res.headersSent) {
        return next(error);
    }
    res.status(error.code || 500);
    res.json({
        message: error.message || 'An unknown error occured!'
    });
});
module.exports = app;
