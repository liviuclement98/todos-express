"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Todo_1 = __importDefault(require("./Todo"));
const { DataTypes } = require('sequelize');
const sequelize = require('../../utils/database/sequelize');
const Activity = sequelize.define('activitie', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    status: {
        type: DataTypes.STRING,
        allowNull: false
    },
    user_okta_id: {
        type: DataTypes.STRING,
        allowNull: false
    }
}, {
    timestamps: false,
});
Activity.hasMany(Todo_1.default, { as: 'toDoList', foreignKey: 'activity_id' });
exports.default = Activity;
