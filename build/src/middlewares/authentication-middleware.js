"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const OktaJwtVerifier = require('@okta/jwt-verifier');
const oktaJwtVerifier = new OktaJwtVerifier({
    issuer: `https://${process.env.OKTA_DOMAIN}/oauth2/default`,
    clientId: process.env.OKTA_CLIENT_ID,
});
const audience = 'api://default';
const authenticationRequired = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const authHeader = req.headers.authorization || '';
    const match = authHeader.match(/Bearer (.+)/);
    if (!match) {
        return res.status(401).send();
    }
    try {
        const accessToken = match[1];
        if (!accessToken) {
            return res.status(401).send();
        }
        const jwt = yield oktaJwtVerifier.verifyAccessToken(accessToken, audience);
        req.userData = {
            email: jwt.claims.sub,
            oktaId: jwt.claims.uid,
        };
        next();
    }
    catch (err) {
        if (err.message.includes('Error while resolving signing key for kid') && err.parsedBody) {
            req.userData = {
                email: err.parsedBody.sub,
                oktaId: err.parsedBody.uid,
            };
            return next();
        }
        return res.status(401).send(err.message);
    }
});
exports.default = authenticationRequired;
