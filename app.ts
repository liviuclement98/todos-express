import {NextFunction, Request, Response} from "express";
import databaseInit from "./src/utils/database/databaseInit";
import authenticationRequired from "./src/middlewares/authentication-middleware";
import autoImportEnvVariables from './src/utils/utils'
import dotenv from 'dotenv'
import express from "express";

dotenv.config();
autoImportEnvVariables();

declare global {
    namespace Express {
        interface Request {
            userData?: any,
        }
    }
}

const app = express();

databaseInit()

const cors = require('cors');
app.use(cors());

//parse body to json
app.use(express.json());

const activityRoutes = require('./src/api/routes/activity-routes');
const userRoutes = require('./src/api/routes/user-routes');

app.use('/api/activities', authenticationRequired, activityRoutes);
app.use('/api/users', userRoutes);

//handle unsupported routes
app.use(() => {
    const error: any = new Error('Could not find this route');
    error.code = 404;
    throw error;
});

// error handling middleware
app.use((error: any, req: Request, res: Response, next: NextFunction) => {
    if (res.headersSent) {
        return next(error);
    }
    res.status(error.code || 500);
    res.json({
        message: error.message || 'An unknown error occured!'
    });
});

module.exports = app;
